package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame {
	JFrame frame;
	JLabel label1,label2;
	JPanel panel1,panel2,panel3;
	JTextArea result;
	JTextField input;
	JButton runbutton,endbutton;
	String str="";
	public JComboBox<String> type;
	
	public Gui(){
		createFrame();
	}
	
	public void createFrame(){
		
		panel1 = new JPanel();
		panel2 = new JPanel();
		
		result = new JTextArea(20,25);
		
		input = new JTextField(10);
		
		runbutton = new JButton("RUN");
		runbutton.setBounds(0,0,150,50);
		
		endbutton = new JButton("END PROGRAM");
		endbutton.setBounds(0,0,150,50);
		
		type = new JComboBox<String>();
		type.setBounds(0,100,200,50);
		type.addItem("Nested Loop 1");
		type.addItem("Nested Loop 2");
		type.addItem("Nested Loop 3");
		type.addItem("Nested Loop 4");
		type.addItem("Nested Loop 5");
		type.setSelectedIndex(0);
		
		panel1.add(result);
		panel2.add(type);	
		panel2.add(input);
		panel2.add(runbutton);
		panel2.add(endbutton);
		
		add(panel1);
		add(panel2);
		 
	}
	
	public void setResult(String str){
		this.str = str;
		result.setText(str);
		}
	
	public String getInput(){
		 return input.getText();
	   }
	
	public void setListener(ActionListener list){
		 endbutton.addActionListener(list);
	   }
	
	public void setListener2(ActionListener list){
		 runbutton.addActionListener(list);
	   }
}
